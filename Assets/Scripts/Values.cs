using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Values : MonoBehaviour
{
    public bool burnt = false;
    public bool onFire = false;
    public float hotness = 0;

    // Increase hotness by argument, then update state based on new value
    public void increaseHotness(float increase)
    {
        hotness += increase;
        if (hotness >= 100)
        {
            onFire = false;
            burnt = true;
            var cubeRenderer = gameObject.GetComponent<Renderer>();
            cubeRenderer.material.SetColor("_Color", Color.black);
            gameObject.layer = LayerMask.NameToLayer("Burnt");
        }
        else if(hotness >= 20)
        {
            onFire = true;
            var cubeRenderer = gameObject.GetComponent<Renderer>();
            cubeRenderer.material.SetColor("_Color", Color.red);
        }
    }

    // Douse a cube that is on fire
    public void douse()
    {
        hotness = 0;
        onFire = false;
        var cubeRenderer = gameObject.GetComponent<Renderer>();
        cubeRenderer.material.SetColor("_Color", Color.green);
    }

    //getters and setters

    public float getHotness()
    {
        return hotness;
    }

    public void setFire(bool value)
    {
        onFire = value;
    }

    public void setBurnt(bool value)
    {
        burnt = value; ;
    }

    public bool getFire()
    {
        return onFire;
    }

    public bool getBurnt()
    {
        return burnt;
    }
}
