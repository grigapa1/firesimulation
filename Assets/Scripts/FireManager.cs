using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireManager : MonoBehaviour
{
    //reference to the wind indicator
    public GameObject arrowPointer;

    //object we are planting
    public GameObject boxPlant;
    public float sizeOfItem = 2;
    public int itemsToPlace = 5000;
    private Vector3 scaleOfBox = new Vector3(5.0f, 5.0f, 5.0f);

    //wind info
    private float windDirection = 0;
    private float windSpeed = 1;

    //game info
    private bool running = false;
    private int mode = 0;

    //spherecast variables
    private List<GameObject> objInRange = new List<GameObject>();
    private Vector3 origin;
    private Vector3 direction;
    public float sphereRadius = 5.0f;
    public float maxDistance = 50.0f;
    public LayerMask layerMask;
    private float currentHitDist;

    //miscellaneous other declarations
    Terrain MyTerrain;
    TerrainData MyTerrainData;
    public LayerMask terrainMask;

    // Update is called once per frame
    void Update()
    {
        //Get mouse input
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (mode == 0) //Create new cube
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, terrainMask))
                {
                    //Debug.Log("Placing cube");
                    GameObject cube = Instantiate(boxPlant, hit.point, boxPlant.transform.rotation);
                    cube.transform.parent = transform;
                    cube.transform.localScale = scaleOfBox;
                    var cubeRenderer = cube.GetComponent<Renderer>();
                    cubeRenderer.material.SetColor("_Color", Color.green);
                }
            }
            else if (mode == 1) // Remove cube
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
                {
                    DestroyImmediate(hit.transform.gameObject);
                }
            }
            else if (mode == 2) // Set cube on fire or douse it
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
                {
                    if (hit.transform.gameObject.GetComponent<Values>().getFire())
                    {
                        hit.transform.gameObject.GetComponent<Values>().douse();
                    }else
                    {
                        hit.transform.gameObject.GetComponent<Values>().increaseHotness(10.0f);
                    }
                    
                }
            }
        }

        // If simulation is running, update spreading of fire
        if (running)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                // Spread fire from each node that is on fire
                if(transform.GetChild(i).gameObject.GetComponent<Values>().onFire == true)
                {
                    transform.GetChild(i).gameObject.GetComponent<Values>().increaseHotness(0.5f); //decay
                    origin = transform.GetChild(i).position;

                    // Get direction of the Wind stick
                    Vector3 windEffect = new Vector3( Mathf.Cos(windDirection), 0.0f, -Mathf.Sin(windDirection));
                    direction = windEffect;
                    direction.Normalize();

                    RaycastHit[] hits = Physics.SphereCastAll(origin, sphereRadius, direction, maxDistance * windSpeed, layerMask, QueryTriggerInteraction.UseGlobal);

                    //directional spread using Wind speed and Wind direction
                    foreach(RaycastHit hit in hits)
                    {
                        if (!hit.transform.gameObject.GetComponent<Values>().getFire())
                        {
                            hit.transform.gameObject.GetComponent<Values>().increaseHotness((maxDistance * windSpeed - hit.distance) / (maxDistance * windSpeed));
                        }
                        else
                        {
                            hit.transform.gameObject.GetComponent<Values>().increaseHotness(((maxDistance * windSpeed - hit.distance) / (maxDistance * windSpeed))/20.0f);
                        }
                    }

                    //passive all-directional fire spread
                    Collider[] hitColliders = Physics.OverlapSphere(origin, 15, layerMask, QueryTriggerInteraction.UseGlobal);
                    foreach (var hitCollider in hitColliders)
                    {
                        if (!hitCollider.gameObject.GetComponent<Values>().getFire())
                        {
                            hitCollider.gameObject.GetComponent<Values>().increaseHotness(0.2f);
                        }
                        else
                        {
                            hitCollider.gameObject.GetComponent<Values>().increaseHotness(0.01f);
                        }
                    }
                }
            }
        }
    }

    // Generate new batch of plants at random locations
    public void Generate()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }

        //layerMask = LayerMask.GetMask("Water");
        int count = 0;
        while (count < itemsToPlace)
        {
            Place();
            count++;
        }
    }

    // Place a cube at random position within the given area
    public void Place()
    {

        MyTerrain = transform.parent.GetComponent<Terrain>();
        MyTerrainData = MyTerrain.terrainData;
        Vector3 size = MyTerrainData.size;
        float itemXSpread = size.x;
        float itemYSpread = 2;
        float itemZSpread = size.z;
        
        Vector3 randPosition = new Vector3(Random.Range(0, itemXSpread), itemYSpread, Random.Range(0, itemZSpread)) + transform.parent.position;

        GameObject cube = Instantiate(boxPlant, randPosition, boxPlant.transform.rotation);
        cube.transform.parent = transform;
        cube.transform.localScale = scaleOfBox;
        var cubeRenderer = cube.GetComponent<Renderer>();
        cubeRenderer.material.SetColor("_Color", Color.green);
    }

    // Clear all current plants
    public void Clear()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }

    // Play/Stop simulation
    public void Simulate()
    {
        running = !running;
        Debug.Log("Running is" + running);
    }

    // Set several randomly selected plants on fire
    public void FireOnOff()
    {
        int rand = Random.Range(1, 5);
        int count = 0;
        while(count < rand)
        {
            int randChild = Random.Range(0, transform.childCount - 1);
            while ((transform.GetChild(randChild).gameObject.GetComponent<Values>().onFire == true) || (transform.GetChild(randChild).gameObject.GetComponent<Values>().burnt == true))
            {
                randChild = Random.Range(0, transform.childCount - 1);
            }
            transform.GetChild(randChild).gameObject.GetComponent<Values>().onFire = true;
            transform.GetChild(randChild).gameObject.GetComponent<Values>().hotness = 50.0f;
            var cubeRenderer = transform.GetChild(randChild).gameObject.GetComponent<Renderer>();
            cubeRenderer.material.SetColor("_Color", Color.red);
            count++;
        }
        
    }

    // Toggle between modes - Adding plants(0), Removing plants(1), Lightning/Extinguising plants(2)
    public void SwitchMode()
    {
        mode = (mode + 1) % 3;
        Debug.Log("Mode " + mode);
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quit");
    }


    public void setWindDirection(float value)
    {
        windDirection = value;
        Vector3 temp = new Vector3(0.0f, value, 0.0f);
        arrowPointer.transform.parent.eulerAngles = temp * 57.2957795131f ;
    }

    public void setWindSpeed(float value)
    {
        windSpeed = value;
    }

    public float getWindDirection()
    {
        return windDirection;
    }

    public float getWindSpeed()
    {
        return windSpeed;
    }
}
